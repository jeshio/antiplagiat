﻿class Author < ActiveRecord::Base
  attr_accessible :detail_info, :name

  validates :name, presence: true, length: { minimum: 5 }, format: {
		with: %r{[[:word:]]+ [[:word:]]+.*}i,
		message: 'Введите имя и фамилию'
	}

	has_many :local_texts, dependent: :destroy
	
end
