class LocalText < ActiveRecord::Base
  attr_accessible :author_id, :text, :title
  belongs_to :author
  has_many :words_pos, dependent: :destroy
  
  validates :author_id, :text, :title, presence: true
  validates :title, length: { minimum: 6 }, uniqueness: true
  validates :text, length: { minimum: 40 }
end
