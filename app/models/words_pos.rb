class WordsPos < ActiveRecord::Base
  attr_accessible :doc_id, :next_word_id, :word_id
  belongs_to :local_text, :word

  def add_words(words, id) # добавление проиндексированных слов
    insert_words_pos = Array.new
    insert_words = Array.new
    exist_words = Word.all.map(&:word)
    words.each do |w|
    	if !exist_words.include?(w)
      	insert_words << "('#{word}', 1)"
      end
    end
    sql = "INSERT INTO words (`word`, `count`) VALUES #{insert_words.join(", ")}"
    ActiveRecord::Base.connection.execute(sql)
    exist_words = Word.all
    words.each_index do |i|
    	word_id = exist_words.find { |e| e[:word] == words[i] }
    	if !words[i + 1].nil?
    		next_word_id = exist_words.find { |e| e[:word] == words[i + 1] }
      	insert_words_pos << "('#{word_id}', #{id}, #{next_word_id})"
      else
      	insert_words_pos << "('#{word_id}', #{id}, NULL)"
      end
    end
    sql = "INSERT INTO words_pos (`word_id`, `local_text_id`, `next_word_id`) VALUES #{insert_words_pos.join(", ")}"
    ActiveRecord::Base.connection.execute(sql)
  end
end
