class Word < ActiveRecord::Base
  attr_accessible :count, :word
  belongs_to :local_text
  validates :word, :count, presence: true
end