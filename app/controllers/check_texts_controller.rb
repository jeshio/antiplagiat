class CheckTextsController < ApplicationController
  # GET /check_texts
  # GET /check_texts.json
  def index
    @check_text = CheckText.new

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @check_texts }
    end
  end

  # POST /check_texts
  # POST /check_texts.json
  def create
    @check_text = CheckText.new(params[:check_text])

    @result = Array.new

    @light_texts = Array.new

    exploded_text = explode_text(@check_text.text)

    @total_words = exploded_text.count

    bench_db = Benchmark.measure { @result = scan_text(exploded_text) }.real

    bench_hl = Benchmark.measure { 
    @result.each_index do |id|
      if !@result[id].nil?
        @light_texts[id] = light_matches(LocalText.find(id).text, @result[id])
        if @id_of_max.nil? or @result[id].count > @result[@id_of_max].count
          @id_of_max = id
          @origin = sprintf('%.2f', (1 - (@result[@id_of_max].size.to_f / @total_words.to_f)) * 100)
        end
      end
    end
    }.real

    @notice = sprintf("Time of choice from DB: %2.5f Ligthing: %2.5f", bench_db, bench_hl)

    respond_to do |format|
      format.html
      format.json { render json: @check_texts }
    end
  end
end
