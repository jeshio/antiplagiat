class ApplicationController < ActionController::Base
  protect_from_forgery

  private

  def split_text_on_words(text)
    text.scan(/[[:word:]]+/)
  end

  def explode_text(text)
    split_text_on_words(text.mb_chars.downcase.to_s)
  end

  def scan_text(text)
  	@result = Array.new
    words_full = Word.all(order:'word')
    words = words_full.map { |f| f[:word] }
    text.each do |word|
      if !(index = words.index(word)).nil?
        for word_i in index..words.rindex(word)
            id = words_full[word_i].local_text_id
            @result[id].nil? ? @result[id] = [word] : 
            @result[id] += [word]
        end
      end
    end
    return @result
  end

  def add_text(text, id)
    words = explode_text(text)
    Word.create.add_words(words, id)
    return words.count
  end

  def light_matches(text, matches)
    return text.gsub(/\b[[^[:word:]\s]]*(#{matches.join("|")})[[^[:word:]\s]]*\b/i, '<span class="label label-warning">\1</span>')
  end
end
