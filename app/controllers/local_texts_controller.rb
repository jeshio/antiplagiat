class LocalTextsController < ApplicationController
  # GET /local_texts
  # GET /local_texts.json
  def index
    @local_texts = LocalText.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @local_texts }
    end
  end

  # GET /local_texts/1
  # GET /local_texts/1.json
  def show
    @local_text = LocalText.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @local_text }
    end
  end

  # GET /local_texts/new
  # GET /local_texts/new.json
  def new
    @local_text = LocalText.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @local_text }
    end
  end

  # GET /local_texts/1/edit
  def edit
    @local_text = LocalText.find(params[:id])
  end

  # POST /local_texts
  # POST /local_texts.json
  def create
    @local_text = LocalText.new(params[:local_text])
    files = params[:local_text][:file]
    if !files.nil?
      @file = files.read
    end

    respond_to do |format|
      if @local_text.save
        words_count = 0
        bench = Benchmark.measure { words_count = add_text(@local_text.text, @local_text.id) }.real

        format.html { redirect_to @local_text, notice: "Text was successfully added. 
          Time of indexing: #{sprintf("%2.3f", bench)} (#{sprintf("%2.5f/word", bench/words_count)})" }
        format.json { render json: @local_text, status: :created, location: @local_text }
      else
        format.html { render action: "new" }
        format.json { render json: @local_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /local_texts/1
  # PUT /local_texts/1.json
  def update
    @local_text = LocalText.find(params[:id])

    respond_to do |format|
      if @local_text.update_attributes(params[:local_text])
        Word.destroy_all(local_text_id: @local_text.id)
        add_text(@local_text.text, @local_text.id)
        format.html { redirect_to @local_text, notice: 'Local text was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @local_text.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /local_texts/1
  # DELETE /local_texts/1.json
  def destroy
    @local_text = LocalText.find(params[:id])
    @local_text.destroy

    respond_to do |format|
      format.html { redirect_to local_texts_url }
      format.json { head :no_content }
    end
  end
end
