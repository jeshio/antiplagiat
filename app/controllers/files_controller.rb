﻿class FilesController < ApplicationController
	def index

	end

	def new
		
	end

	def create
		@result = Array.new
		if !params[:files].nil?
			params[:files].each do |f|
				name = f.original_filename
				@result += [{ name: name[0..(name.rindex('.').nil? ? -1 : name.rindex('.'))], text: f.read }]
			end
		elsif !params[:url].nil?
			require 'open-uri'
			
			next_url = params[:url]
			begin
				doc = Nokogiri::HTML(open(next_url, 'User-Agent' => 'Mozilla')).to_html
				articles = doc.scan(/#{params[:reg_details]}/i)

				articles.each do |link|
					art = Nokogiri::HTML(open(link[0], 'User-Agent' => 'Mozilla')).to_html
					art_name = art.scan(/#{params[:reg_title]}/i)[0][0]
					art_text = art.scan(/#{params[:reg_text]}/i)
					@result += [{ name: art_name, text: art_text }]
					@result.count < params[:art_count].to_i ? nil : break
				end
				
				@result.count < params[:art_count].to_i ? nil : break
				(next_url = doc.scan(params[:reg_pagination])).count.zero? ? break : nil
			end while true
		else
			count_added = 0
			if !params[:text].nil?
				params[:name].each_index do |i|
					if params[:check_text].include?(params[:name][i])
						@local_text = LocalText.new(title: params[:name][i], author_id: params[:author_id][i],
																				text: params[:text][i])
						if @local_text.save
							count_added += 1
							add_text(@local_text.text, @local_text.id)
						end
					end
				end
			end
			respond_to do |format|
	    		format.html { redirect_to action: 'index', notice: count_added.to_s + ' текстов добавлено' }
	      		format.json { render json: "index", status: :created, location: "index" }
    		end
  		end
	end
end
