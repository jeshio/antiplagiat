class CreateCheckTexts < ActiveRecord::Migration
  def change
    create_table :check_texts do |t|
      t.text :text

      t.timestamps
    end
  end
end
