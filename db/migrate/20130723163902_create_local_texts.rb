class CreateLocalTexts < ActiveRecord::Migration
  def change
    create_table :local_texts do |t|
      t.string :title
      t.text :text
      t.integer :author_id

      t.timestamps
    end
  end
end
