class CreateWordsPos < ActiveRecord::Migration
  def change
    create_table :words_pos do |t|
      t.integer :word_id
      t.integer :doc_id
      t.integer :next_word_id

      t.timestamps
    end
  end
end
