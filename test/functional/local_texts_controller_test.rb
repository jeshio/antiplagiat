require 'test_helper'

class LocalTextsControllerTest < ActionController::TestCase
  setup do
    @local_text = local_texts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:local_texts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create local_text" do
    assert_difference('LocalText.count') do
      post :create, local_text: { author_id: @local_text.author_id, text: @local_text.text, title: @local_text.title }
    end

    assert_redirected_to local_text_path(assigns(:local_text))
  end

  test "should show local_text" do
    get :show, id: @local_text
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @local_text
    assert_response :success
  end

  test "should update local_text" do
    put :update, id: @local_text, local_text: { author_id: @local_text.author_id, text: @local_text.text, title: @local_text.title }
    assert_redirected_to local_text_path(assigns(:local_text))
  end

  test "should destroy local_text" do
    assert_difference('LocalText.count', -1) do
      delete :destroy, id: @local_text
    end

    assert_redirected_to local_texts_path
  end
end
