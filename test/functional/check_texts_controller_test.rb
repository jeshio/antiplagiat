require 'test_helper'

class CheckTextsControllerTest < ActionController::TestCase
  setup do
    @check_text = check_texts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should be checked" do
    post :create, check_text: { text: @check_text.text }
    assert_response :success

    refute_nil :resulte
  end
end
